import './App.css';
import Home from './pages/home';
import RecipeEdit from './pages/recipe_edit';
import ShoppingList from './pages/shopping_list';
import { Route, Switch, Link } from 'react-router-dom';
import React from 'react';

function App() {
  return (
    <React.Fragment>
      <header>
        <nav className="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow">
          <div className="container">
            <Link className="navbar-brand" to="/">Recipe planner</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="navbar-collapse collapse d-sm-inline-flex justify-content-between">
              <ul className="navbar-nav flex-grow-1">
                <li className="nav-item">
                  <Link className="nav-link text-dark" to="/">Recipes</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link text-dark" to="/recipe/add">Add recipe</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link text-dark" to="/shopping/list">Create shopping list</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div className="container pb-3 flex-area">
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/recipe/add" component={(props) => <RecipeEdit {...props} add={true}/>}/>
          <Route path="/recipe/edit/:name" component={(props) => <RecipeEdit {...props} add={false}/>}/>
          <Route exact path="/shopping/list" component={ShoppingList}/>
        </Switch>
      </div>
      <footer className="border-top footer text-muted pb-2 pt-2">
        <div className="container">
          &copy; 2021 - P. Grzegorczyk
        </div>
      </footer>
    </React.Fragment>
  );
}

export default App;
