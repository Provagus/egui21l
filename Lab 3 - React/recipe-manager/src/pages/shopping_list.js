import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function ShoppingList(){
    document.title = "Shopping List";
    const [recipeNames, setRecipeNames] = useState([]);
    const [recipesData, setRecipesData] = useState({});
    const [ingredientsStr, setIngredientsStr] = useState([]);
    
    useEffect(() => {
        //initial obtaining all recipes
        axios.get('/recipes').then(
            response => {
                //array for names and amount of each recipe
                let recipeNamesArray = [];
                for(let recipeName in response.data){
                    recipeNamesArray.push({name: recipeName, amount: 0});
                }
                //https://stackoverflow.com/questions/8996963/how-to-perform-case-insensitive-sorting-in-javascript
                recipeNamesArray.sort(function (a, b) {
                    return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
                });
                //passing response data to function responsible for processing format into something more useful
                InitialCalculateIngredients(response.data);
                setRecipeNames([...recipeNamesArray]);
            }
        );
    }, []);

    function InitialCalculateIngredients(recipes){
        //this function will be called once only when shopping list is loaded
        //it takes response from server and passes it to local storage
        let convertedRecipes = {};
        //iterating over recipes
        for(let name in recipes){
            let recipe = {};
            let values = recipes[name];
            recipe.recipe = values.recipe;
            let ingArray = [];
            //iterating over ingredients
            for(let ingredient in values){
                if(ingredient === "recipe"){
                    continue;
                }
                let currIng = {};
                currIng.name = ingredient;
                let amAndUnit = values[ingredient];
                //obtaining amount since number is first
                let amount = amAndUnit.split(" ")[0];
                amount = parseFloat(amount);
                currIng.amount = amount;
                //removing number and using rest as unit
                let unit = amAndUnit.split(" ").splice(1).join(" ");
                currIng.unit = unit;
                ingArray.push(currIng);
            }
            //modified format, ingredients are under ingredients key in an array
            recipe.ingredients = ingArray;
            convertedRecipes[name] = recipe;
        }
        //updating storage
        setRecipesData(convertedRecipes);
    }

    function CalculateIngredients(){
        //called after each increase or decrease of values
        //recalcutaes values
        
        //sum of ingredients will be disctionary holding for each used ingredient dictionary for each unit with amount
        let sumOfIngredients = {};
        for(let index in recipeNames){
            //iterating over recipes
            if(recipeNames[index].amount === 0){
                //not interested in recipes with value 0
                continue;
            }
            let recipeDetails = recipesData[recipeNames[index].name];
            for(let ingredientIndex in recipeDetails.ingredients){
                //iterating over recipe ingredients
                let ingredient = recipeDetails.ingredients[ingredientIndex];
                //creating proper keys in dictionaries and their values if they were not in dictionary before
                if(!(ingredient.name in sumOfIngredients)){
                    sumOfIngredients[ingredient.name] = {};
                }
                if(!(ingredient.unit in sumOfIngredients[ingredient.name])){
                    sumOfIngredients[ingredient.name][ingredient.unit] = 0;
                }
                //adding ingredients
                sumOfIngredients[ingredient.name][ingredient.unit] += ingredient.amount * recipeNames[index].amount;
            }
        }
        let ingredientsString = [];
        var sortedNames = [];
        //sorting ingredients by names
        for(let ingredientName in sumOfIngredients){
            sortedNames[sortedNames.length] = ingredientName;
        }
        sortedNames.sort(function (a, b) {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        });
        //creating string array which will be displayed
        for(let index in sortedNames){
            let ingredientName = sortedNames[index];
            for(let ingredientUnit in sumOfIngredients[ingredientName]){
                ingredientsString.push(ingredientName + " - " + sumOfIngredients[ingredientName][ingredientUnit].toFixed(2).toString() + " " + ingredientUnit);
            }
        }
        //updating ingredients
        setIngredientsStr([...ingredientsString]);
    }

    function handleIncreaseAmount(index){
        let recipesArray = recipeNames;
        recipesArray[index].amount++;
        setRecipeNames([...recipesArray]);
        CalculateIngredients(); //recalculating ingredients
    }

    function handleDecreaseAmount(index){
        let recipesArray = recipeNames;
        //checking if can decrease
        if(recipesArray[index].amount <= 0){
            return;
        }
        recipesArray[index].amount--;
        setRecipeNames([...recipesArray]);
        CalculateIngredients(); //recalculating ingredients
    }

    //HTML section
    return(
        <React.Fragment>
        {recipeNames.map((obj, index) => {
            return(
            <div className="recipe p-2" key={index}>
                <span style={{marginTop: "auto", marginBottom: "auto"}} >{obj.name}</span>
                <span>
                    <span className="btn btn-primary" onClick={e => handleDecreaseAmount(index)}><i className="fas fa-minus"></i></span>
                    <span className="mx-3" style={{marginTop: "auto", marginBottom: "auto"}}>{obj.amount}</span>
                    <span className="btn btn-primary" onClick={e => handleIncreaseAmount(index)}><i className="fas fa-plus"></i></span>
                </span>
            </div>
            )
        })}
        <div className="my-3 border-top">{ingredientsStr.map((ingredient, index) => {
            return(
                <div>{ingredient}</div>
            )
        })}</div>
        <Link to="/" className="btn btn-success">Back</Link>
        </React.Fragment>
    );
}

export default ShoppingList;