import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useHistory } from 'react-router-dom';

function RecipeEdit(props){
    document.title = "Recipe editor";
    const [name, setName] = useState("");
    const [recipe, setRecipe] = useState("");
    const [ingredients, setIngredients] = useState([]);

    const history = useHistory();
    let oldName = undefined;
    if(props.add === false){
        oldName = props.match.params.name;
    }
    useEffect(() => {
        //Old name is defined so it is editting
        if(oldName !== undefined){
            setName(oldName);
            axios.get('/recipes').then(
                response => {
                    let recipes = response.data;
                    let values = recipes[oldName];
                    setRecipe(values.recipe.join("\n"));
                    let ingArray = [];
                    for(let ingredient in values){
                        if(ingredient === "recipe"){
                            continue;
                        }
                        let currIng = {};
                        currIng.name = ingredient;
                        let amAndUnit = values[ingredient];
                        let amount = amAndUnit.split(" ")[0];
                        amount = parseFloat(amount);
                        currIng.amount = amount;
                        let unit = amAndUnit.split(" ").splice(1).join(" ");
                        currIng.unit = unit;
                        ingArray.push(currIng);
                    }
                    setIngredients([...ingArray]);
                }
            );
        }
    }, [oldName]);

    function handleSubmit(event){
        event.preventDefault();

        //Making proper object ready to write
        let data = {};
        data["recipe"] = recipe.split(/\r?\n/);
        for (let i = 0; i < ingredients.length; i++) {
            const ingredient = ingredients[i];
            ingredient.name = ingredient.name.trim();
            if(ingredient.name === "recipe"){
                //Ingredient can't be named recipe
                alert(`Ingredient name "recipe" is not allowed`);
                return;
            }
            if(ingredient.name in data){
                //error duplicated
                alert(`Duplicated ingredient name: ${ingredient.name}`);
                return;
            }
            //changing data into required save format and trimming spaces
            data[ingredient.name] = ingredient.amount.toString() + " " + ingredient.unit.trim();
        }
        console.log(data);
        setName(name.trim());

        axios.post(`/recipe/edit/${oldName === undefined ? name : oldName}/${name}`, data).then(
            //pushing change and redirecting to home page
            response => {
                history.push('/');
            }
        )
        .catch(error => {
            console.error(error.response.status);
            console.error(error.response);
            alert(`Server error! Status code: ${error.response.status}`);
        });
    }

    //changing name of recipe
    function handleChangeName(event){
        setName(event.target.value);
    }

    //changing recipe steps
    function handleChangeRecipe(event){
        setRecipe(event.target.value);
    }

    function AddIngredient(){
        //append new ingredient to array
        setIngredients(ingredients => [...ingredients, {
            "name" : "",
            "amount" : 0,
            "unit" : ""
        }]);
        console.log(ingredients);
    }

    //modifying unit of ingredient
    function ModifyUnit(event, index){ 
        let tmpIng = ingredients;
        tmpIng[index].unit = event.target.value;
        setIngredients([...tmpIng]);
    }

    //modifying ingredient amount
    function ModifyAmount(event, index){ 
        let tmpIng = ingredients;
        tmpIng[index].amount = parseFloat(event.target.value);
        setIngredients([...tmpIng]);
    }

    //modify ingredient name
    function ModifyName(event, index){
        let tmpIng = ingredients;
        tmpIng[index].name = event.target.value;
        setIngredients([...tmpIng]);
    }

    //remove specific ingredient
    function RemoveElement(event, index){
        let tmpIng = ingredients;
        tmpIng.splice(index, 1);
        setIngredients([...tmpIng]);
    }

    //HTML section, rendering edit form
    return(
        <React.Fragment>
        <div className="text-left">
            <form className="form-inline" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>Recipe name</label>
                    <input type="text" name="name" onChange={handleChangeName} value={name} className="form-control" placeholder="Enter recipe name" required/>
                </div>
                <div className="form-group">
                    <label>Recipe steps</label>
                    <textarea className="form-control" name="recipe" onChange={handleChangeRecipe} value={recipe} rows="5" placeholder="Enter recipe steps" required></textarea>
                </div>
                <div className="form-group">
                    <label>Ingredients</label><br></br>
                    <button className="btn btn-primary my-2" type="button" onClick={AddIngredient}>Add ingredient</button>
                    {ingredients.map((ingredient, index) => {
                        return(<div className="row mb-2" key={index}>
                            <div className="col">
                                <input type="text" className="form-control" value={ingredient.name} onChange={e => {ModifyName(e, index)}} required></input>
                            </div>
                            <div className="col">
                                <input type="number" className="form-control" min="0" step="0.01" value={ingredient.amount} onChange={e => {ModifyAmount(e, index)}} required></input>
                            </div>
                            <div className="col">
                                <input type="text" className="form-control" value={ingredient.unit} onChange={e => {ModifyUnit(e, index)}} required></input>
                            </div>
                            <div className="col">
                                <button className="btn btn-danger" type="button" onClick={e => {RemoveElement(e, index)}}>Remove</button>
                            </div>
                        </div>)
                    })}
                </div>
                <button type="submit" className="btn btn-success my-2">Submit</button>
                <Link className="btn btn-danger m-2" to="/">Cancel</Link>
            </form>
        </div>
        </React.Fragment>
    );
}

export default RecipeEdit;