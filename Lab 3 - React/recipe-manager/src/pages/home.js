import axios from "axios";
import React from "react";
import { Link } from 'react-router-dom';

class RecipeList extends React.Component{
    //initial values
    constructor(props){
        super(props);
        this.state = {
            recipes: [],
            fetching: true
        }
    }

    //loading recipes
    componentDidMount() {
    axios.get('/recipes')
        .then(response => {
            //Pushing recipe names into array
            let recipeNames = [];
            for(let recipe in response.data){
                recipeNames.push(recipe);
            }
            //https://stackoverflow.com/questions/8996963/how-to-perform-case-insensitive-sorting-in-javascript
            recipeNames.sort(function (a, b) {
                return a.toLowerCase().localeCompare(b.toLowerCase());
            });
            this.setState({
                recipes: recipeNames,
                fetching: false
            });
        })
        .catch(error => {
            console.error(error.response.status);
            console.error(error.response);
            alert(`Server error! Status code: ${error.response.status}`); //error notification if can't some error occures
        });
    }

    //Delete recipe function
    DeleteRecipe(name){
        axios.get(`/recipe/delete/${name}`)
        .then(response => {
            let recipeNames = this.state.recipes;
            let foundIndex = recipeNames.indexOf(name);
            if(foundIndex !== -1){
                recipeNames.splice(foundIndex, 1);
            }
            this.setState({
                recipes: recipeNames,
                fetching: false
            });
        })
        .catch(error => {
            console.error(error.response.status);
            console.error(error.response);
            alert(`Server error! Status code: ${error.response.status}`);
        });
    }

    render(){
        document.title = "Recipe Manager";
        return(
            <React.Fragment>
                {this.state.fetching ? <div>Fetching...</div> : ""}
                {this.state.recipes.map((recipe, index) => {
                    return(
                        <div key={index} className={`${index !== 0 ? "border-top" : ""} py-3 recipe`}>
                            <span style={{marginTop: "auto", marginBottom: "auto"}} className="px-2">{recipe}</span>
                            <span>
                            <Link to={`/recipe/edit/${recipe}`} className="btn btn-link"><i className="fas fa-edit"></i></Link>
                            <span className="mx-1 btn btn-link" style={{color: "red"}} onClick={e => this.DeleteRecipe(recipe)}><i className="fas fa-trash"></i></span>
                            </span>
                        </div>
                    )
                })}
            </React.Fragment>
    )}
}

export default RecipeList;