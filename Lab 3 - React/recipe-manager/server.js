const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');

app.use(express.static(path.join(__dirname, "build")));
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.get('/', (req, res) => {
    //serving react elements
    res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.post('/recipe/edit/:name/:newName', (req, res) => {
    //editing recipes
    let name = req.params.name;
    let newName = req.params.newName;
    let jsonData = {};
    if(fs.existsSync('recipes.json')){
        let rData = fs.readFileSync('recipes.json');
        jsonData = JSON.parse(rData);
    }
    //removing old element if exists
    delete jsonData[name];
    //writing new element
    jsonData[newName] = req.body;
    fs.writeFileSync('recipes.json', JSON.stringify(jsonData));
    res.sendStatus(200);
});

app.get('/recipe/delete/:name', (req, res) => {
    //deleting recipe
    let name = req.params.name;
    let jsonData = {};
    if(fs.existsSync('recipes.json')){
        let rData = fs.readFileSync('recipes.json');
        jsonData = JSON.parse(rData);
    }
    delete jsonData[name];
    fs.writeFileSync('recipes.json', JSON.stringify(jsonData));
    res.sendStatus(200);
});

app.get('/recipes', (req, res) => {
    //serving recipes.json file
    let jsonData = {};
    if(fs.existsSync('recipes.json')){
        let rData = fs.readFileSync('recipes.json');
        jsonData = JSON.parse(rData);
    }
    res.json(jsonData);
});

app.listen(5000);