using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Lab_2___MVC.Models;

namespace Lab_2___MVC.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult InvalidIngredient(string message){
            ViewData["Error_message"] = message;
            return View();
        }

        public IActionResult DuplicateName(string message){
            ViewData["Error_message"] = message;
            return View();
        }
    }
}