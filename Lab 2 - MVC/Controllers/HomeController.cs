﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Lab_2___MVC.Models;

namespace Lab_2___MVC.Controllers
{
    public class HomeController : Controller
    {
        const string FILE_PATH = "recipes.json";
        const string REGEX_FIRST_NUMBER = @"^\d+(\.\d+)?(?=(\s)+)";
        const string REGEX_LAST_NUMBER_BEFORE_UNIT = @"(?<=\s)\d+(\.\d+)?(?=(\s)+[^\s])(?!.*(?<=\s)\d+(\.\d+)?(?=(\s)+[^\s]))";

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            List<RecipeModel> recipes = getDataFromJSON();
            return View(recipes);
        }

        public IActionResult RecipeView(string name){
            /*
            Function for returning view for recipe add or edit
            */
            //Add recipe case
            if(name == null){
                ViewData["addRecipe"] = true;
            }
            //Edit recipe case
            else{
                ViewData["addRecipe"] = false;
                List<RecipeModel> recipes = getDataFromJSON();
                //Safe check if recipe exists
                if(!RecipeListContains(recipes, name)){
                    ViewData["addRecipe"] = true;
                    return View();
                }
                //Obtaining recipe SortedDictionary
                RecipeModel recipe = null;
                foreach(RecipeModel i in recipes){
                    if(i.name == name){
                        recipe = i;
                        break;
                    }
                }
                //Assigning recipe name
                ViewData["recipeName"] = name;
                //Recreating recipe names
                string resultStringRecipeSteps = "";
                foreach(string text in recipe.steps){
                    resultStringRecipeSteps += text + "\n";
                }
                ViewData["recipeSteps"] = resultStringRecipeSteps.TrimEnd();
                //Recreating recipe string
                string resultStringIngredients = "";
                foreach(IngredientModel ingredient in recipe.ingredients){
                    resultStringIngredients += ingredient.name + " " + ingredient.amount.ToString(CultureInfo.InvariantCulture) + " " + ingredient.unit + "\n";
                }
                ViewData["ingredients"] = resultStringIngredients.TrimEnd();
            }
            return View();
        }

        [HttpPost]
        public void RecipeAdd(string name, string recipe, string ingredients){
            /*
            Function for adding recipe
            */
            //reading from file
            List<RecipeModel> recipes = getDataFromJSON();
            if(RecipeListContains(recipes, name)){
                Response.Redirect(Url.Action("DuplicateName", "Error", new {message = "Recipe with such name already exists"}));
                return;
            }
            RecipeModel recipeModel;
            try{
                recipeModel = getRecipeModelFromString(name, recipe, ingredients);
                //Recipe SortedDictionary
                SortedDictionary<string, dynamic> recipeDict = new SortedDictionary<string, dynamic>();
                recipeDict.Add("recipe", recipeModel.steps);
                //Adding recipe to other recipes
                recipes.Add(recipeModel);
                //saving to file
                saveDataToJSON(recipes);
            }
            catch(InvalidIngredient e){
                Response.Redirect(Url.Action("InvalidIngredient", "Error", new {message = e.Message}));
                return;
            }
            catch(DuplicateDeteted e){
                Response.Redirect(Url.Action("DuplicateName", "Error", new {message = e.Message}));
                return;
            }
            //Redirect to main page
            Response.Redirect(Url.Action("Index", "Home"));
        }

        [HttpPost]
        public void RecipeEdit(string name, string recipe, string ingredients, string oldName){
            /*
            Function for editting recipe
            */
            //reading from file
            List<RecipeModel> recipes = getDataFromJSON();
            if(name != oldName && RecipeListContains(recipes, name)){
                Response.Redirect(Url.Action("DuplicateName", "Error", new {message = "Recipe with such name already exists"}));
                return;
            }
            RecipeModel recipeModel;
            try{
                recipeModel = getRecipeModelFromString(name, recipe, ingredients);
                //Replacing recipe with new one
                foreach(RecipeModel model in recipes){
                    if(model.name == oldName){
                        recipes.Remove(model);
                        break;
                    }
                }
                recipes.Add(recipeModel);
                //saving to file
                saveDataToJSON(recipes);
            }
            catch(InvalidIngredient e){
                Response.Redirect(Url.Action("InvalidIngredient", "Error", new {message = e.Message}));
                return;
            }
            catch(DuplicateDeteted e){
                Response.Redirect(Url.Action("DuplicateName", "Error", new {message = e.Message}));
                return;
            }
            //Redirect to main page
            Response.Redirect(Url.Action("Index", "Home"));
        }

        public void RecipeDelete(string name){
            /*
            Function for removing recipe
            */
            //Reading from JSON
            List<RecipeModel> recipes = getDataFromJSON();
            //Removing
            foreach(RecipeModel model in recipes){
                if(name == model.name){
                    recipes.Remove(model);
                    break;
                }
            }
            //saving to file
            saveDataToJSON(recipes);
            Response.Redirect(Url.Action("Index", "Home"));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private List<RecipeModel> getDataFromJSON(){
            SortedDictionary<string, SortedDictionary<string, dynamic>> data;
            if(System.IO.File.Exists(FILE_PATH)){
                string jsonString = System.IO.File.ReadAllText(FILE_PATH);
                data = JsonSerializer.Deserialize<SortedDictionary <string, SortedDictionary<string, dynamic>>>(jsonString);
            }
            else{
                data = new SortedDictionary<string, SortedDictionary<string, dynamic>>();
            }
            List<RecipeModel> recipes = new List<RecipeModel>();
            foreach(string key in data.Keys){
                RecipeModel recipeModel = new RecipeModel();
                recipeModel.name = key;
                //Recipe steps parsing
                List<string> recipeSteps = new List<string>();
                foreach(JsonElement text in data[key]["recipe"].EnumerateArray()){
                    recipeSteps.Add(text.GetString());
                }
                recipeModel.steps = recipeSteps;
                //Ingredient parsing
                List<IngredientModel> ingredients = new List<IngredientModel>();
                foreach(string key2 in data[key].Keys){
                    if(key2 == "recipe"){
                        continue;
                    }
                    IngredientModel ingredient = new IngredientModel();
                    ingredient.name = key2;
                    string amountAndUnit = data[key][key2].GetString();
                    Regex reg = new Regex(REGEX_FIRST_NUMBER, RegexOptions.Compiled);
                    MatchCollection matches = reg.Matches(amountAndUnit);
                    if(matches.Count > 0){
                        ingredient.amount = float.Parse(matches[0].Value, CultureInfo.InvariantCulture);
                        ingredient.unit =  amountAndUnit.Substring(matches[0].Length).Trim();
                    }
                    ingredients.Add(ingredient);
                }
                recipeModel.ingredients = ingredients;
                recipes.Add(recipeModel);
            }
            return recipes;
        }

        private void saveDataToJSON(List<RecipeModel> data){
            SortedDictionary<string, SortedDictionary<string, dynamic>> recipes = new SortedDictionary<string, SortedDictionary<string, dynamic>>();
            foreach(RecipeModel recipeModel in data){
                //Recipe SortedDictionary
                SortedDictionary<string, dynamic> recipeDict = new SortedDictionary<string, dynamic>();
                recipeDict.Add("recipe", recipeModel.steps);
                //Adding ingredients
                foreach(IngredientModel ingredient in recipeModel.ingredients){
                    string ingredientAmount = ingredient.amount.ToString(CultureInfo.InvariantCulture) + " " + ingredient.unit;
                    recipeDict.Add(ingredient.name, ingredientAmount);
                }
                //Adding recipe to other recipes
                recipes.Add(recipeModel.name, recipeDict);
            }
            string jsonString = JsonSerializer.Serialize(recipes);
            System.IO.File.WriteAllText(FILE_PATH, jsonString);
        }

        private RecipeModel getRecipeModelFromString(string name, string recipe, string ingredients){
            //Cleaning any whitespaces at beginning / end, which could cause issues
            name = name.Trim();
            recipe = recipe.Trim();
            ingredients = ingredients.Trim();
            //https://stackoverflow.com/questions/1547476/easiest-way-to-split-a-string-on-newlines-in-net/1547483
            RecipeModel recipeModel = new RecipeModel();
            recipeModel.name = name;
            List<string> splittedRecipe = recipe.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None).ToList<string>();
            recipeModel.steps = splittedRecipe;
            recipeModel.ingredients = new List<IngredientModel>();
            //Handling ingeredients
            List<string> splittedIngredients = ingredients.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None).ToList<string>();
            //This regex finds number, with or without decimal part followed and ended with whitespace, after which there is unit
            //And there are no more matching patterns after finding that number
            //Should return either 1 or 0 matches
            Regex reg = new Regex(REGEX_LAST_NUMBER_BEFORE_UNIT, RegexOptions.Compiled);
            //Ingredient handling
            HashSet<string> ingredientNames = new HashSet<string>();
            for(int i = 0; i < splittedIngredients.Count; i++){
                IngredientModel ingredientModel = new IngredientModel();
                string processed = splittedIngredients[i];
                MatchCollection matches = reg.Matches(processed);
                if(matches.Count > 0){
                    ingredientModel.amount = float.Parse(matches[0].Value, CultureInfo.InvariantCulture);
                    ingredientModel.name = processed.Substring(0, matches[0].Index).Trim();
                    if(ingredientNames.Contains(ingredientModel.name)){
                        throw new DuplicateDeteted("Duplicated ingredient names");
                    }
                    ingredientNames.Add(ingredientModel.name);
                    ingredientModel.unit =  processed.Substring(matches[0].Index + matches[0].Length).Trim();
                    if(ingredientModel.name.Length == 0){
                        throw new InvalidIngredient("Empty name");
                    }
                    if(ingredientModel.unit.Length == 0){
                        throw new InvalidIngredient("Empty unit");
                    }
                    recipeModel.ingredients.Add(ingredientModel);
                }
                else{
                    throw new InvalidIngredient("No amount found");
                }
            }
            return recipeModel;
        }

        public IActionResult SelectRecipesView(){
            List<RecipeModel> recipes = getDataFromJSON();
            return View(recipes);
        }

        public IActionResult IngredientList(){
            List<RecipeModel> recipes = getDataFromJSON();
            //Dictionary holding for ingredient name as key, dictionary holding unit as key and amount as value
            SortedDictionary<string, SortedDictionary<string, float>> ingredients = new SortedDictionary<string, SortedDictionary<string, float>>();
            foreach(string key in Request.Form.Keys){
                int amount = int.Parse(Request.Form[key]);
                //Check next if recipe not used
                if(amount == 0){
                    continue;
                }
                RecipeModel recipe = null;
                //Assigning recipe to tmp model
                foreach(RecipeModel model in recipes){
                    if(model.name == key){
                        recipe = model;
                    }
                }
                //Check if recipe exists
                if(recipe == null){
                    continue;
                }
                //Summing up ingredients
                foreach(IngredientModel ingredient in recipe.ingredients){
                    //Initialising ingredient and unit if not in dictionary
                    if(!ingredients.ContainsKey(ingredient.name)){
                        ingredients.Add(ingredient.name, new SortedDictionary<string, float>());
                    }
                    if(!ingredients[ingredient.name].ContainsKey(ingredient.unit)){
                        ingredients[ingredient.name].Add(ingredient.unit, 0);
                    }
                    //Adding extra ingredients
                    ingredients[ingredient.name][ingredient.unit] += ingredient.amount * amount;
                }
            }
            return View(ingredients);
        }

        private bool RecipeListContains(List<RecipeModel> recipeList, string name){
            bool contains = false;
            foreach(RecipeModel model in recipeList){
                if(model.name == name){
                    contains = true;
                    break;
                }
            }
            return contains;
        }
    }

    //Invalid ingredient exception class
    public class InvalidIngredient : Exception{
        public InvalidIngredient(string message): base(message){}
    }

    //Duplicated element exception class
    public class DuplicateDeteted : Exception{
        public DuplicateDeteted(string message): base(message){}
    }
}
