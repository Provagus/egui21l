using System;
using System.Collections.Generic;

namespace Lab_2___MVC.Models
{
    public class RecipeModel
    {
        public string name { get; set; }
        public List<string> steps { get; set; }
        public List<IngredientModel> ingredients { get; set; }
    }
}
