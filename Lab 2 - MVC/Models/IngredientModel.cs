using System;
using System.Collections.Generic;

namespace Lab_2___MVC.Models
{
    public class IngredientModel
    {
        public string name { get; set; }
        public float amount { get; set; }
        public string unit { get; set; }
    }
}
