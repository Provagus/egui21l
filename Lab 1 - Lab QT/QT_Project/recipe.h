#ifndef RECIPE_H
#define RECIPE_H
#include <QVector>
#include <QStringList>
#include "ingredient.h"

//Class for holding recipes
class Recipe
{
public:
    Recipe();
    ~Recipe();

    void setName(const QString&);
    QString getName() const;

    bool operator<(const Recipe&) const;
    bool operator>(const Recipe&) const;
    bool operator==(const Recipe&) const;

    QVector<Ingredient> ingredientList;
    QStringList recipeSteps;
private:
    QString _name;
};

#endif // RECIPE_H
