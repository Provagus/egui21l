#ifndef INGREDIENTLISTDIALOG_H
#define INGREDIENTLISTDIALOG_H

#include <QDialog>
#include <QStringListModel>

namespace Ui {
class IngredientListDialog;
}

//Dialog to display required ingredient list
class IngredientListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IngredientListDialog(QString &ingredients, QWidget *parent = nullptr);
    ~IngredientListDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::IngredientListDialog *ui;
    QString *_ingredients;
};

#endif // INGREDIENTLISTDIALOG_H
