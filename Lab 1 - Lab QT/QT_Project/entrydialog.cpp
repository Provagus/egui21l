#include "entrydialog.h"
#include "ui_entrydialog.h"
#include "errorinformationdialog.h"
#include <algorithm>
#include <QDebug>

const QString INGREDIENT_FIELDS_MESSAGE = "Please fill in all ingredient input fields";
const QString GENERAL_ERROR_MESSAGE = "An error has occured.";
const QString MISSING_RECIPE_FIELDS_MESSAGE = "Please fill in all required fields for the recipe";
const QString DUPLICATE_INGREDIENTS_MESSAGE = "Ingredient is already on the list";
const QString DUPLICATE_RECIPES_MESSAGE = "Recipe with same name already exists";

//Contructor for new recipe
EntryDialog::EntryDialog(QWidget *parent, QVector<Recipe> *recipes) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);
    //validator setup
    validator = new QDoubleValidator(this);
    validator->setNotation(QDoubleValidator::StandardNotation);
    ui->amountIngEdit->setValidator(validator);
    //setting up ingredient list and its view
    _ingredientList = new QVector<Ingredient>();
    _ingredientStringListModel = new QStringListModel(this);
    ingredientStringList = new QStringList();
    _ingredientStringListModel->setStringList(*ingredientStringList);
    ui->ingredientsListView->setModel(_ingredientStringListModel);
    _recipes = recipes;
    _editingRecipe = false;
    connect(this, SIGNAL(updatedIngredients()), this, SLOT(updateIngredientsView()));
}

//Constructor for editting recipe
EntryDialog::EntryDialog(QWidget *parent, QVector<Recipe> *recipes, const int &selectedRecipe) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);
    //validator setup
    validator = new QDoubleValidator(this);
    validator->setNotation(QDoubleValidator::StandardNotation);
    ui->amountIngEdit->setValidator(validator);
    //inserting recipe data into fields
    QVector<Recipe>::Iterator selectedRecipeIter(recipes->begin() + selectedRecipe);
    _ingredientList = new QVector<Ingredient>(selectedRecipeIter->ingredientList);
    ui->recipeNameInput->setText(selectedRecipeIter->getName());
    QString steps = selectedRecipeIter->recipeSteps.join("\n");
    ui->recipeTextInput->document()->setPlainText(steps);
    _ingredientStringListModel = new QStringListModel(this);
    //setting up ingredient list and its view
    ingredientStringList = new QStringList();
    _ingredientStringListModel->setStringList(*ingredientStringList);
    ui->ingredientsListView->setModel(_ingredientStringListModel);
    updateIngredientsView();
    _recipes = recipes;
    _editingRecipe = true;
    _selectedRecipe = selectedRecipeIter;
    connect(this, SIGNAL(updatedIngredients()), this, SLOT(updateIngredientsView()));
}

EntryDialog::~EntryDialog()
{
    delete ui;
    delete _ingredientList;
    delete _ingredientStringListModel;
    delete ingredientStringList;
    delete validator;
}

void EntryDialog::on_cancelButton_clicked()
{
    //cancel, we can exit without changes
    this->close();
    delete this;
}

void EntryDialog::on_ingredientsListView_clicked(const QModelIndex &index)
{
    _selectedIngredient = index.row();
    ui->nameIngEdit->setText(_ingredientList->at(_selectedIngredient).getName());
    //Replacing . with , to be consistent in presenting data
    ui->amountIngEdit->setText(QString::number(_ingredientList->at(_selectedIngredient).getAmount()).replace(".", ","));
    ui->unitIngEdit->setText(_ingredientList->at(_selectedIngredient).getUnit());
    //enabling edit and remove buttons
    ui->editIngredientButton->setEnabled(true);
    ui->removeIngredientButton->setEnabled(true);
}

void EntryDialog::on_addIngredientButton_clicked()
{
    //Reading fields
    //Replacing comma with dot, since validator uses comma and to float requires dot
    float amount = ui->amountIngEdit->text().replace(",", ".").toFloat();
    QString name = ui->nameIngEdit->text();
    QString unit = ui->unitIngEdit->text();
    //Checking if fields are empty
    if (ui->unitIngEdit->text() == "" ||
        ui->nameIngEdit->text() == "" ||
        ui->unitIngEdit->text() == ""){
        ErrorInformationDialog *errorDialog = new ErrorInformationDialog(this, INGREDIENT_FIELDS_MESSAGE);
        errorDialog->exec();
        return;
    }
    //Adding new ingredient
    Ingredient newIngredient(name, amount, unit);
    //Preventing adding ingredient with same name twice
    //JSON keys must be unique
    if(std::binary_search(_ingredientList->begin(), _ingredientList->end(), newIngredient)){
        ErrorInformationDialog *errorDialog = new ErrorInformationDialog(this, DUPLICATE_INGREDIENTS_MESSAGE);
        errorDialog->exec();
        return;
    }
    _ingredientList->append(newIngredient);
    std::sort(_ingredientList->begin(), _ingredientList->end());
    emit updateIngredientsView();
    //Cleaning fields
    ui->amountIngEdit->clear();
    ui->nameIngEdit->clear();
    ui->unitIngEdit->clear();
    //disabling edit and remove buttons
    ui->editIngredientButton->setEnabled(false);
    ui->removeIngredientButton->setEnabled(false);
}

void EntryDialog::updateIngredientsView(){
    ingredientStringList->clear();
    //Preparing visible text in list view field
    for(int i = 0; i < _ingredientList->size(); i++){
        QString display = _ingredientList->at(i).getName();
        display += " - " + QString::number(_ingredientList->at(i).getAmount()).replace(".", ",");
        display += " " + _ingredientList->at(i).getUnit();
        ingredientStringList->append(display);
    }
    //Refreshing view
    _ingredientStringListModel->setStringList(*ingredientStringList);
}

void EntryDialog::on_removeIngredientButton_clicked()
{
    //Removing ingredient
    _ingredientList->remove(_selectedIngredient);
    emit updateIngredientsView();
    //Cleaning fields
    ui->amountIngEdit->clear();
    ui->nameIngEdit->clear();
    ui->unitIngEdit->clear();
    //disabling buttons
    ui->editIngredientButton->setEnabled(false);
    ui->removeIngredientButton->setEnabled(false);
}

void EntryDialog::on_editIngredientButton_clicked()
{
    //Reading fields
    //Replacing comma with dot, since validator uses comma and to float requires dot
    float amount = ui->amountIngEdit->text().replace(",", ".").toFloat();
    QString name = ui->nameIngEdit->text();
    QString unit = ui->unitIngEdit->text();
    //Checking if fields are empty
    if (ui->unitIngEdit->text() == "" ||
        ui->nameIngEdit->text() == "" ||
        ui->unitIngEdit->text() == ""){
        ErrorInformationDialog *errorDialog = new ErrorInformationDialog(this, INGREDIENT_FIELDS_MESSAGE);
        errorDialog->exec();
        return;
    }
    //Editing
    (*_ingredientList)[_selectedIngredient].setName(name);
    (*_ingredientList)[_selectedIngredient].setAmount(amount);
    (*_ingredientList)[_selectedIngredient].setUnit(unit);
    std::sort(_ingredientList->begin(), _ingredientList->end());
    emit updateIngredientsView();
    //Cleaning
    ui->amountIngEdit->clear();
    ui->nameIngEdit->clear();
    ui->unitIngEdit->clear();
    //disabling buttons
    ui->editIngredientButton->setEnabled(false);
    ui->removeIngredientButton->setEnabled(false);
}

void EntryDialog::on_saveButton_clicked()
{
    if(!_recipes){
        ErrorInformationDialog *errorDialog = new ErrorInformationDialog(this, GENERAL_ERROR_MESSAGE);
        errorDialog->exec();
        qErrnoWarning("Invalid recipe vector. Nullptr");
        return;
    }
    QString name, recipeSteps;
    name = ui->recipeNameInput->text();
    recipeSteps = ui->recipeTextInput->toPlainText();
    //Checking if recipe name and steps are inserted
    //I assume that in some cases ingredients are not needed
    if(name == "" ||
        recipeSteps == ""){
        ErrorInformationDialog *errorDialog = new ErrorInformationDialog(this, MISSING_RECIPE_FIELDS_MESSAGE);
        errorDialog->exec();
        return;
    }
    //Each line is separate recipe step
    QStringList recipeStepsList;
    recipeStepsList = recipeSteps.split("\n");
    //Editing vs new one
    if(_editingRecipe){
        //Editing ingredient
        _selectedRecipe->setName(name);
        _selectedRecipe->ingredientList = *_ingredientList;
        _selectedRecipe->recipeSteps = recipeStepsList;
    }
    else{
        //New ingredient
        Recipe newRecipe;
        newRecipe.setName(name);
        newRecipe.ingredientList = *_ingredientList;
        newRecipe.recipeSteps = recipeStepsList;
        //JSON requires unique keys, recipes must have different names
        if(std::binary_search(_recipes->begin(), _recipes->end(), newRecipe)){
            ErrorInformationDialog *errorDialog = new ErrorInformationDialog(this, DUPLICATE_RECIPES_MESSAGE);
            errorDialog->exec();
            return;
        }
        _recipes->append(newRecipe);
    }
    //Emiting update signal
    emit updatedRecipe();
    //we can close window now
    this->close();
    delete this;
}
