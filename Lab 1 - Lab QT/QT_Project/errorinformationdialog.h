#ifndef ERRORINFORMATIONDIALOG_H
#define ERRORINFORMATIONDIALOG_H

#include <QDialog>

namespace Ui {
class ErrorInformationDialog;
}

//Very simple dialog to display error messages
class ErrorInformationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ErrorInformationDialog(QWidget *parent = nullptr);
    ErrorInformationDialog(QWidget *parent = nullptr, const QString &errorMessage = "");
    ~ErrorInformationDialog();

private slots:
    void on_okButton_clicked();

private:
    Ui::ErrorInformationDialog *ui;
};

#endif // ERRORINFORMATIONDIALOG_H
