#include "ingredientlistdialog.h"
#include "ui_ingredientlistdialog.h"
#include <QStringListModel>

IngredientListDialog::IngredientListDialog(QString &ingredients, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IngredientListDialog)
{
    ui->setupUi(this);
    _ingredients = &ingredients;
    ui->ingredientListView->setText(ingredients);
}

IngredientListDialog::~IngredientListDialog()
{
    delete ui;
}

void IngredientListDialog::on_pushButton_clicked()
{
    this->close();
    delete this;
}
