#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "entrydialog.h"
#include "shoppingmanager.h"
#include <algorithm>
#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _recipeNames = new QStringList();
    _recipeModel = new QStringListModel();
    _recipeModel->setStringList(*_recipeNames);
    ui->recipesListView->setModel(_recipeModel);
    //loading data from json file
    loadFromJSON();
    generateRecipeView();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _recipeNames;
    delete _recipeModel;
}

void MainWindow::on_addButton_clicked()
{
    EntryDialog* addDialog = new EntryDialog(this, &_recipes);
    connect(addDialog, SIGNAL(updatedRecipe()), this, SLOT(updateRecipeView()));
    addDialog->exec();
}

void MainWindow::generateRecipeView(){
    std::sort(_recipes.begin(), _recipes.end());
    _recipeNames->clear();
    for(int i = 0; i < _recipes.size(); i++){
        _recipeNames->append(_recipes[i].getName());
    }
    _recipeModel->setStringList(*_recipeNames);
}


void MainWindow::updateRecipeView(){
    std::sort(_recipes.begin(), _recipes.end());
    _recipeNames->clear();
    for(int i = 0; i < _recipes.size(); i++){
        _recipeNames->append(_recipes[i].getName());
    }
    _recipeModel->setStringList(*_recipeNames);
    //saving to JSON, calling this function occurs when data could change
    saveToJSON();
}

void MainWindow::on_recipesListView_clicked(const QModelIndex &index)
{
    _selected = index.row();
    ui->editButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
}

void MainWindow::on_removeButton_clicked()
{
    _recipes.remove(_selected);
    updateRecipeView();
    //cleaning
    ui->editButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}

void MainWindow::on_editButton_clicked()
{
    EntryDialog* editDialog = new EntryDialog(this, &_recipes, _selected);
    connect(editDialog, SIGNAL(updatedRecipe()), this, SLOT(updateRecipeView()));
    editDialog->exec();
}

void MainWindow::saveToJSON(){
    QFile saveFile(QStringLiteral("recipes.json"));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Can't open file");
        return;
    }
    //Preapring data for saving
    QJsonObject jsonObject;
    //iterating over recipes
    for(int i = 0; i < _recipes.size(); i++){
        QJsonObject recipe;
        QJsonArray recipeSteps;
        //iterating over recipe steps
        for(int j = 0; j < _recipes[i].recipeSteps.size(); j++){
            recipeSteps.append(_recipes[i].recipeSteps[j]);
        }
        recipe["recipe"] = recipeSteps;
        //iterating over ingredients
        for(int j = 0; j < _recipes[i].ingredientList.size(); j++){
            QString ingName = _recipes[i].ingredientList[j].getName();
            QString ingUnit = _recipes[i].ingredientList[j].getUnit();
            float ingAmount = _recipes[i].ingredientList[j].getAmount();
            QString ingInput;
            //making proper format for storing amounts and units
            ingInput = QString::number(ingAmount) + " " + ingUnit;
            recipe[ingName] = ingInput;
        }
        jsonObject[_recipes[i].getName()] = recipe;
    }
    //Saving
    saveFile.write(QJsonDocument(jsonObject).toJson());
}

void MainWindow::loadFromJSON(){
    QFile loadFile(QStringLiteral("recipes.json"));

    //will return if file doesn't exists
    //most likely to run on first load
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Can't open file");
        return;
    }

    QByteArray readData = loadFile.readAll();

    //Reading from file
    QJsonDocument loadDoc(QJsonDocument::fromJson(readData));
    QJsonObject jsonObject = loadDoc.object();
    //Processing input
    //Iterating over single recipes
    for(int i = 0; i < jsonObject.keys().size(); i++){
        Recipe newRecipe;
        QString recipeName = jsonObject.keys()[i];
        newRecipe.setName(recipeName);
        auto currentRecipe = jsonObject.value(jsonObject.keys()[i]).toObject();
        //Iterating over ingredients (and recipe array)
        for(int j = 0; j < currentRecipe.keys().size(); j++){
            if(currentRecipe.keys()[j] == "recipe"){
                auto recipeSteps = currentRecipe.value("recipe").toArray();
                //iterating over recipe steps
                for(int k = 0; k < recipeSteps.size(); k++){
                    newRecipe.recipeSteps.append(recipeSteps[k].toString());
                }
            }
            else{
                QString ingredientText = currentRecipe.value(currentRecipe.keys()[j]).toString();
                QString ingredientName = currentRecipe.keys()[j];
                Ingredient newIngredient;
                newIngredient.setName(ingredientName);
                //obtaining units and amount
                float ingredientAmount = ingredientText.split(" ").takeFirst().toFloat();
                newIngredient.setAmount(ingredientAmount);
                auto unitList = ingredientText.split(" ");
                unitList.removeFirst();
                QString ingredientUnit = unitList.join(" ");
                newIngredient.setUnit(ingredientUnit);
                newRecipe.ingredientList.append(newIngredient);
            }
        }
        _recipes.append(newRecipe);
    }
}

void MainWindow::openShoppingManager(){
    ShoppingManager* shoppingDialog = new ShoppingManager(_recipes, this);
    shoppingDialog->exec();
}
