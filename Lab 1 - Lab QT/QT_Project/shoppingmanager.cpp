#include "shoppingmanager.h"
#include "ui_shoppingmanager.h"
#include "ingredientlistdialog.h"
#include <algorithm>
#include <QDebug>

ShoppingManager::ShoppingManager(const QVector<Recipe> &recipes, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShoppingManager)
{
    ui->setupUi(this);
    _recipes = &recipes;
    for(int i = 0; i < _recipes->size(); i++){
        _recipeNames.append(_recipes->at(i).getName());
    }
    _recipeNamesModel.setStringList(_recipeNames);
    ui->recipiesListView->setModel(&_recipeNamesModel);

    _selectedRecipeNamesModel.setStringList(_selectedRecipeNames);
    ui->selectedRecipiesListView->setModel(&_selectedRecipeNamesModel);
}

ShoppingManager::~ShoppingManager()
{
    delete ui;
}

void ShoppingManager::on_recipiesListView_clicked(const QModelIndex &index)
{
    _selectedRecipeAdd = index.row();
    ui->addButton->setEnabled(true);
}

void ShoppingManager::on_addButton_clicked()
{
    _selectedRecipes.append(&_recipes->at(_selectedRecipeAdd));
    std::sort(_selectedRecipes.begin(), _selectedRecipes.end());
    updateSelectedRecipesView();
}

void ShoppingManager::on_cancelButton_clicked()
{
    this->close();
    delete this;
}

void ShoppingManager::updateSelectedRecipesView(){
    _selectedRecipeNames.clear();
    for(int i = 0; i < _selectedRecipes.size(); i++){
        _selectedRecipeNames.append(_selectedRecipes[i]->getName());
    }
    _selectedRecipeNamesModel.setStringList(_selectedRecipeNames);
}

void ShoppingManager::on_selectedRecipiesListView_clicked(const QModelIndex &index)
{
    ui->removeButton->setEnabled(true);
    _selectedRecipeRemove = index.row();
}

void ShoppingManager::on_removeButton_clicked()
{
    ui->removeButton->setEnabled(false);
    _selectedRecipes.remove(_selectedRecipeRemove);
    updateSelectedRecipesView();
}

void ShoppingManager::on_calculateButton_clicked()
{
    _ingredientsList.clear();
    //mapped ingredient name into mapped unit to amount
    QMap<QString, QMap<QString, float>> requiredIngredients;
    //Iterating over selected recipes
    for(int i = 0; i < _selectedRecipes.size(); i++){
        auto ingredientList = &_selectedRecipes[i]->ingredientList;
        //Iterating over ingredients
        for(int j = 0; j < ingredientList->size(); j++){
            //increasing amount for each ingredient and its unit
            //new fields have 0 by default, therefore I do not have to set 0 for new ingredients, units
            requiredIngredients[ingredientList->at(j).getName()][ingredientList->at(j).getUnit()] += ingredientList->at(j).getAmount();
        }
    }
    //preparing ingredient list strings for each ingredient and unit
    for(auto it = requiredIngredients.begin(); it != requiredIngredients.end(); it++){
        for(auto it2 = it->begin(); it2 != it->end(); it2++){
            QString singleIngredient = it.key() + " - ";
            singleIngredient += QString::number(it2.value()) + " " + it2.key();
            _ingredientsList.append(singleIngredient);
        }
    }
    std::sort(_ingredientsList.begin(), _ingredientsList.end());
    qDebug() << _ingredientsList;
    //preparing finall string
    _ingredientsText = "";
    for(int i = 0; i < _ingredientsList.size(); i++){
        _ingredientsText += _ingredientsList[i] + "\n";
    }
    IngredientListDialog *ingredientsDialog = new IngredientListDialog(_ingredientsText, this);
    ingredientsDialog->exec();
    return;
}
