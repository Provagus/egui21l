#include "recipe.h"

Recipe::Recipe()
{

}

Recipe::~Recipe(){}

void Recipe::setName(const QString &name){
    _name = name;
}

QString Recipe::getName() const{
    return _name;
}

//comparisons based on names
bool Recipe::operator<(const Recipe &cmp) const{
    auto result = QString::compare(this->_name, cmp._name, Qt::CaseInsensitive);
    return result < 0 ? true: false;
}

bool Recipe::operator>(const Recipe &cmp) const{
    auto result = QString::compare(this->_name, cmp._name, Qt::CaseInsensitive);
    return result > 0 ? true: false;
}

bool Recipe::operator==(const Recipe &cmp) const{
    return this->_name == cmp._name;
}
