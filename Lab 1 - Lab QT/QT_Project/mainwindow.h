#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include "recipe.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

//Class for main window
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //Function to refresh recipe list view
    void generateRecipeView();

private slots:
    void on_addButton_clicked();

    void updateRecipeView();

    void on_recipesListView_clicked(const QModelIndex &index);

    void on_removeButton_clicked();

    void on_editButton_clicked();

    //Function for opening shopping manager from context menu
    void openShoppingManager();

private:
    Ui::MainWindow *ui;
    QVector<Recipe> _recipes;
    //string list and model for holding and diplaying recipe views
    QStringList *_recipeNames;
    QStringListModel *_recipeModel;
    //int holding information which recipe is selected
    int _selected;

    //handling JSON file
    void saveToJSON();
    void loadFromJSON();
};
#endif // MAINWINDOW_H
