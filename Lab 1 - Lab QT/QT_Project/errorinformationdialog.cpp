#include "errorinformationdialog.h"
#include "ui_errorinformationdialog.h"

ErrorInformationDialog::ErrorInformationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ErrorInformationDialog)
{
    ui->setupUi(this);
}

ErrorInformationDialog::ErrorInformationDialog(QWidget *parent, const QString &errorMessage) :
    QDialog(parent),
    ui(new Ui::ErrorInformationDialog)
{
    ui->setupUi(this);
    ui->errorMessage->setText(errorMessage);
}

ErrorInformationDialog::~ErrorInformationDialog()
{
    delete ui;
}

void ErrorInformationDialog::on_okButton_clicked()
{
    this->close();
    delete this;
}
