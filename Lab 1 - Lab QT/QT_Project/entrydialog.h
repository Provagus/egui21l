#ifndef ENTRYDIALOG_H
#define ENTRYDIALOG_H

#include <QDialog>
#include <QStringListModel>
#include <QValidator>
#include "ingredient.h"
#include "recipe.h"

namespace Ui {
class EntryDialog;
}

//Dialog to add / edit recipe
class EntryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EntryDialog(QWidget *parent, QVector<Recipe> *recipes);
    explicit EntryDialog(QWidget *parent, QVector<Recipe> *recipes, const int &selectedRecipe);
    ~EntryDialog();

    QStringList *ingredientStringList;

private slots:
    void on_cancelButton_clicked();

    void on_ingredientsListView_clicked(const QModelIndex &index);

    void on_addIngredientButton_clicked();

    void on_removeIngredientButton_clicked();

    void on_editIngredientButton_clicked();

    void on_saveButton_clicked();

    //Function for refreshing ingredients list view
    void updateIngredientsView();

signals:
    void updatedIngredients();
    void updatedRecipe();

private:
    Ui::EntryDialog *ui;
    //Ingredient vector
    QVector<Ingredient> *_ingredientList;
    //String list model attached to the list view
    QStringListModel *_ingredientStringListModel;
    //Validator for amount input
    QDoubleValidator* validator;
    //Selected ingredient on the view list
    int _selectedIngredient;
    //Recipes vector
    QVector<Recipe> *_recipes;
    //Bool holding information if we add or edit recipe
    bool _editingRecipe;
    //Iterator to selected recipe, used when editting
    QVector<Recipe>::Iterator _selectedRecipe;
};

#endif // ENTRYDIALOG_H
