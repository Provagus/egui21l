#ifndef INGREDIENT_H
#define INGREDIENT_H
#include "QString"

//Ingredient class
class Ingredient
{
public:
    Ingredient();
    Ingredient(const QString &name, const float &amount, const QString &unit);
    ~Ingredient();
    void setName(const QString&);
    void setUnit(const QString&);
    void setAmount(const float&);
    QString getName() const;
    QString getUnit() const;
    float getAmount() const;

    //Basic operators to allow sorting and binary search
    bool operator<(const Ingredient&) const;
    bool operator==(const Ingredient&) const;
    bool operator>(const Ingredient&) const;
private:
    QString _name, _unit;
    float _amount;
};

#endif // INGREDIENT_H
