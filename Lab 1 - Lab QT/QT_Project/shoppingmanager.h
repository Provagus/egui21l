#ifndef SHOPPINGMANAGER_H
#define SHOPPINGMANAGER_H

#include <QDialog>
#include <QStringListModel>
#include "recipe.h"

namespace Ui {
class ShoppingManager;
}

//Shopping manager dialog class
class ShoppingManager : public QDialog
{
    Q_OBJECT

public:
    explicit ShoppingManager(const QVector<Recipe> &recipes, QWidget *parent = nullptr);
    ~ShoppingManager();

private slots:
    void on_recipiesListView_clicked(const QModelIndex &index);

    void on_addButton_clicked();

    void on_cancelButton_clicked();

    void updateSelectedRecipesView();

    void on_selectedRecipiesListView_clicked(const QModelIndex &index);

    void on_removeButton_clicked();

    void on_calculateButton_clicked();

private:
    Ui::ShoppingManager *ui;
    //pointer to recipes
    const QVector<Recipe> *_recipes;
    //handling and displaying recipes
    QStringList _recipeNames;
    QStringListModel _recipeNamesModel;
    //handling and displaying selected recipes
    QVector<const Recipe*> _selectedRecipes;
    QStringList _selectedRecipeNames;
    QStringListModel _selectedRecipeNamesModel;
    //int storing which recipes in both views are currently selected
    int _selectedRecipeAdd, _selectedRecipeRemove;
    //string to display and string list (to sort them before displaying)
    QStringList _ingredientsList;
    QString _ingredientsText;
};

#endif // SHOPPINGMANAGER_H
