#include "ingredient.h"

Ingredient::Ingredient()
{

}

Ingredient::Ingredient(const QString &name, const float &amount, const QString &unit){
    _name = name;
    _amount = amount;
    _unit = unit;
}

Ingredient::~Ingredient(){}

//getters and setters
void Ingredient::setName(const QString& name){ _name = name; }
void Ingredient::setUnit(const QString& unit){ _unit = unit;}
void Ingredient::setAmount(const float& amount){ _amount = amount; }
QString Ingredient::getName() const{ return _name; }
QString Ingredient::getUnit() const{ return _unit; }
float Ingredient::getAmount() const{ return _amount; }

//basic comparison, based on names
bool Ingredient::operator<(const Ingredient &cmp) const{
    auto result = QString::compare(this->_name, cmp._name, Qt::CaseInsensitive);
    return result < 0 ? true: false;
}

bool Ingredient::operator==(const Ingredient &cmp) const{
    return this->_name == cmp._name;
}

bool Ingredient::operator>(const Ingredient &cmp) const{
    auto result = QString::compare(this->_name, cmp._name, Qt::CaseInsensitive);
    return result > 0 ? true: false;
}
